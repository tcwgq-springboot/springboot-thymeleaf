package com.changgou.springbootthymeleaf.controller;

import com.changgou.springbootthymeleaf.bean.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.*;

/**
 * @author tcwgq
 * @since 2022/6/10 15:35
 */
@Slf4j
@Controller
@RequestMapping("/test")
public class DemoController {
    @GetMapping("/hello")
    public String demo(Model model, Long id) {
        log.info("id={}", id);
        // 集合数据
        List<User> users = new ArrayList<>();
        users.add(new User(1, "张三", "深圳"));
        users.add(new User(2, "李四", "北京"));
        users.add(new User(3, "王五", "武汉"));
        model.addAttribute("users", users);

        // Map定义
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("No", "123");
        dataMap.put("address", "深圳");
        model.addAttribute("dataMap", dataMap);

        // 存储一个数组
        String[] names = {"张三", "李四", "王五"};
        model.addAttribute("names", names);

        // 日期
        model.addAttribute("now", new Date());

        //if条件
        model.addAttribute("age",22);

        model.addAttribute("message", "nice to meet you!");
        return "demo";
    }

}
